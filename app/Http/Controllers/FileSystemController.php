<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class FileSystemController extends Controller
{
    public function download()
    {
        //git checkout all to undo all undocumented change
        $process = Process::fromShellCommandline('git checkout .');
        $process->run();
        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        //git pull, update all files
        $process = Process::fromShellCommandline('git pull origin main');
        $process->run();
        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $result = [
            'command' => 'git pull origin main',
            'result' => $process->getOutput(),
            'commandList' => [],
        ];

        // file location at root folder
        foreach (file(base_path('update.txt')) as $line) {
            $result['commandList'][] = ['command' => trim(preg_replace('/\s\s+/', ' ', $line))];
        }

        return $result;
    }

    public function update(Request $request)
    {
        $process = Process::fromShellCommandline($request['command']);
        $process->run();

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $result = [
            'command' => $request['command'],
            'result' => $process->getOutput(),
        ];

        return $result;
    }

    public function getIp()
    {
        return [
            'ip' => gethostbyname(gethostname()),
        ];
    }
}
