<?php

namespace App\Http\Controllers;

use App\Http\Resources\SubmitCollection;
use App\Models\StockopnameSubmit;

class HomeController extends Controller
{
    /**
     * Test HomeController@index
     * show stockopname submit where id is null
     *
     * @return collection query table stockopname_submits, where stockopname_id null, has many detail, with count detail
     *
     */
    public function index()
    {
        $result = StockopnameSubmit::
            whereNull('stockopname_id')
                ->with('detail')
                ->withCount('detail')
                ->orderBy('created_at', 'DESC')
                ->paginate();

        return new SubmitCollection($result);
    }
}
