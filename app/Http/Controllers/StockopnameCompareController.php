<?php

namespace App\Http\Controllers;

use App\Models\Stockopname;
use App\Models\StockopnameCompare;
use App\Models\StockopnameDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StockopnameCompareController extends Controller
{
    /**
     * Test StockopnameSubmitController@update
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\StockopnameCompare  $request
     * @param  \App\Models\StockopnameCompare  $stockopnameCompare
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StockopnameCompare $stockopnameCompare)
    {
        return DB::transaction(function () use ($request, $stockopnameCompare) {
            $validated = $request->validate([
                'oldProductCode' => 'nullable|string',
                'newProductCode' => 'nullable|string',
                'note' => 'nullable|string',
            ]);

            if (isset($validated['newProductCode'])) {
                StockopnameDetail::where('product_code', $validated['oldProductCode'])->update(['product_code' => strtoupper($validated['newProductCode'])]);
            } else {
                $stockopnameCompare->note = $validated['note'];
                $stockopnameCompare->save();
            }

            return $stockopnameCompare;
        });
    }

    public function createReport($stockopname_id, $state = 'view')
    {
        return DB::transaction(function () use ($stockopname_id, $state) {
            $stockopname_compare = StockopnameCompare::where('stockopname_id', $stockopname_id)->get();
            $compare_exist = count($stockopname_compare);
            if (!$compare_exist || $state == 'update') {
                StockopnameCompare::getNewComparison($stockopname_id, $stockopname_compare);
            }

            $stockopname = Stockopname::find($stockopname_id)->load('submit', 'compare');
            $category = StockopnameCompare::getCategorizedStockopname($stockopname->compare);

            return [
                'categories' => $category,
                'stockopname' => $stockopname,
            ];
        });
    }

    public function print(Request $request, $stockopname_id, $state = 'view')
    {
        $today = Carbon::now()->translatedFormat('l, d F Y');
        $stockopname = Stockopname::find($stockopname_id)->load('submit', 'compare');

        $category = StockopnameCompare::getCategorizedStockopname($stockopname->compare, 'print');

        foreach ($category as $key => $categorizedData) {
            $sort = explode(' ', $request[$categorizedData['category']]);
            if (count($categorizedData['sortedResult']) > 1) {
                switch ($sort[0]) {
                    case 'code':
                        $sort[1] == 'up' ?
                            usort($categorizedData['sortedResult'], function ($a, $b) {
                                return strnatcmp($a['product_code'], $b['product_code']);
                            }) :
                            usort($categorizedData['sortedResult'], function ($a, $b) {
                                return -strnatcmp($a['product_code'], $b['product_code']);
                            });
                        break;
                    case 'qty':
                        $sort[1] == 'up' ?
                            usort($categorizedData['sortedResult'], function ($a, $b) {
                                return $a['stockopname'] <=> $b['stockopname'];
                            }) :
                            usort($categorizedData['sortedResult'], function ($a, $b) {
                                return -($a['stockopname'] <=> $b['stockopname']);
                            });
                        break;
                    case 'stock':
                        $sort[1] == 'up' ?
                            usort($categorizedData['sortedResult'], function ($a, $b) {
                                return $a['stockdb'] <=> $b['stockdb'];
                            }) :
                            usort($categorizedData['sortedResult'], function ($a, $b) {
                                return -($a['stockdb'] <=> $b['stockdb']);
                            });
                        break;
                    case 'diff':
                        $sort[1] == 'up' ?
                            usort($categorizedData['sortedResult'], function ($a, $b) {
                                return ($a['stockopname'] - $a['stockdb']) <=> ($b['stockopname'] - $b['stockdb']);
                            }) :
                            usort($categorizedData['sortedResult'], function ($a, $b) {
                                return -(($a['stockopname'] - $a['stockdb']) <=> ($b['stockopname'] - $b['stockdb']));
                            });
                        break;
                }
                $category[$key]['sortedResult'] = $categorizedData['sortedResult'];
            }
        }

        $pdf = app('dompdf.wrapper')->loadView('pdf.stockopname', [
            'date' => $stockopname->created_at->isoFormat('dddd, D MMMM Y'),
            'today' => $today,
            'category' => $category,
        ]);
        $pdf->setPaper('a4', 'portrait');
        $dom_pdf = $pdf->getDomPDF();
        $canvas = $dom_pdf->get_canvas();
        $x = 520; //x coordinate
        $y = 800; //y coordinate
        $text = 'Lembar {PAGE_NUM} / {PAGE_COUNT}';
        $text_font = null;
        $text_size = 10;
        $canvas->page_text($x, $y, $text, $text_font, $text_size, [0, 0, 0]);

        if ($state == 'view') {
            return view('pdf.stockopname', [
                'date' => $stockopname->created_at->isoFormat('dddd, D MMMM Y'),
                'today' => $today,
                'category' => $category,
            ]);
        } elseif ($state == 'viewPdf') {
            return $pdf->stream('laporan-stockopname-' . $stockopname->created_at->isoFormat('dddd, D MMMM Y') . '.pdf');
        } else {
            return $pdf->download('laporan-stockopname-' . $stockopname->created_at->isoFormat('dddd, D MMMM Y') . '.pdf');
        }
    }
}
