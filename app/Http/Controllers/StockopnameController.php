<?php

namespace App\Http\Controllers;

use App\Http\Resources\ApiCollection;
use App\Models\Stockopname;
use App\Models\StockopnameCompare;
use App\Models\StockopnameDetail;
use App\Models\StockopnameSubmit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StockopnameController extends Controller
{
    /**
     * Test StockopnameController@index
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = Stockopname::
            withCount('submit')
                ->withCount('detail')
                ->orderBy('created_at', 'DESC')
                ->paginate(9);

        return new ApiCollection($result);
    }

    /**
     * Test StockopnameController@store
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return DB::transaction(function () use ($request) {
            $validated = $request->validate([
                'stockopname_id' => 'nullable|exists:stockopnames,id',
            ]);

            if (isset($validated['stockopname_id'])) {
                $stockopname = Stockopname::find($request->stockopname_id);
            } else {
                $stockopname = Stockopname::whereRaw('cast(created_at as date) = cast(now() as date)')->first();
            }
            if (!isset($stockopname)) {
                $stockopname = new Stockopname();
                $stockopname->number = 'SO/' . date('d-m-Y');
                $stockopname->save();
            } else {
                $compares = StockopnameCompare::where('stockopname_id', $stockopname->id)->get();
                $ids = $compares->pluck('id')->toArray();
                StockopnameCompare::destroy($ids);
            }
            StockopnameSubmit::whereNull('stockopname_id')->update(['stockopname_id' => $stockopname->id]);

            return ['id' => $stockopname->id, 'created_at' => $stockopname->created_at];
        });
    }

    /**
     * Test StockopnameController@show
     * Display the specified resource.
     *
     * @param  \App\Models\Stockopname  $stockopname
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Stockopname $stockopname)
    {
        $filter = isset($request->filter) ? $request->filter : '';
        $result = StockopnameSubmit::
            where('stockopname_id', $stockopname->id)
                ->withCount('detail')
                ->whereHas('detail', function ($query) use ($filter) {
                    $query->where('product_code', 'like', $filter . '%');
                })
                ->orderBy('created_at', 'DESC')
                ->paginate(9);

        return new ApiCollection($result);
    }

    /**
     * Test StockopnameController@destroy
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Stockopname  $stockopname
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        return DB::transaction(function () {
            $details = StockopnameSubmit::with('detail')->whereNull('stockopname_id')->get();
            if ($details->count() > 0) {
                $ids = [];
                foreach ($details as $detail) {
                    $ids = array_merge($ids, $detail->detail->pluck('id')->toArray());
                }
                StockopnameDetail::destroy($ids);
                StockopnameSubmit::whereNull('stockopname_id')->delete();

                return 'Success';
            } else {
                return 'Data is empty';
            }
        });
    }

    /**
     * Test StockopnameController@getListDate
     * Get list of stockopname's date of this month
     *
     * @return \Illuminate\Http\Response
     */
    public function getListDate()
    {
        return Stockopname::whereDate('created_at', '>=', new Carbon('first day of this month'))->orderBy('created_at', 'desc')->get();
    }
}
