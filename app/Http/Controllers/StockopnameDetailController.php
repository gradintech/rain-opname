<?php

namespace App\Http\Controllers;

use App\Models\StockopnameDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StockopnameDetailController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function store(Request $request)
    {
        return DB::transaction(function () use ($request) {
            $validated = $request->validate([
                'stockopname_submit_id' => 'required|integer',
                'product_code' => 'required|string',
                'new_product_code' => 'required|string',
                'qty' => 'required|integer',
            ]);

            StockopnameDetail::where('product_code', $validated['product_code'])->where('stockopname_submit_id', $validated['stockopname_submit_id'])->delete();
            $details = [];
            for ($i = 0;$i < $validated['qty'];$i++) {
                $details[] = [
                    'stockopname_submit_id' => $validated['stockopname_submit_id'],
                    'product_code' => strtoupper($validated['new_product_code']),
                    'created_at' => now(),
                    'updated_at' => now(),
                ];
            }
            DB::table('stockopname_details')->insert($details);

            return 'Success';
        });
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\StockopnameDetail  $stockopnameDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        return DB::transaction(function () use ($request) {
            $validated = $request->validate([
                'stockopname_submit_id' => 'required|integer',
                'product_code' => 'required|string',
            ]);
            StockopnameDetail::where('product_code', $validated['product_code'])->where('stockopname_submit_id', $validated['stockopname_submit_id'])->delete();

            return 'Success';
        });
    }
}
