<?php

namespace App\Http\Controllers;

use App\Http\Resources\ApiCollection;
use App\Models\StockopnameDetail;
use App\Models\StockopnameSubmit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StockopnameSubmitController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function store(Request $request)
    {
        return DB::transaction(function () use ($request) {
            $validated = $request->validate([
                'username' => 'required|string',
                'items' => 'required|array',
            ]);

            $submit = new StockopnameSubmit();
            $submit->username = $validated['username'];
            $submit->save();

            $details = array_map(function ($item) {
                $detail = new StockopnameDetail();
                $detail->product_code = strtoupper($item);

                return $detail;
            }, $validated['items']);
            $submit->detail()->saveMany($details);
            // event(new StockopnameSubmitEvent($submit, $request->items));
            return $submit->id;
        });
    }

    /**
     * Display stockopname_submits where id x, has many detail, with count detail.
     *
     * @param  \App\Models\StockopnameSubmit  $stockopnameSubmit
     * @return \Illuminate\Http\Response
     */
    public function show(StockopnameSubmit $stockopnameSubmit)
    {
        $result = StockopnameSubmit::where('id', $stockopnameSubmit->id)
            ->with('detailGrouped', 'stockopname')
            ->orderBy('created_at', 'DESC')
            ->get();

        return new ApiCollection($result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\StockopnameSubmit  $request
     * @param  \App\Models\StockopnameSubmit  $stockopnameSubmit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StockopnameSubmit $stockopnameSubmit)
    {
        return DB::transaction(function () use ($request, $stockopnameSubmit) {
            $validated = $request->validate([
                'username' => 'required|string',
            ]);

            $stockopnameSubmit->username = $validated['username'];
            $stockopnameSubmit->save();

            return $stockopnameSubmit;
        });
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\StockopnameSubmit  $stockopnameSubmit
     * @return \Illuminate\Http\Response
     */
    public function destroy(StockopnameSubmit $stockopnameSubmit)
    {
        return DB::transaction(function () use ($stockopnameSubmit) {
            StockopnameDetail::where('stockopname_submit_id', $stockopnameSubmit->id)->delete();
            $stockopnameSubmit->delete();

            return 'success';
        });
    }
}
