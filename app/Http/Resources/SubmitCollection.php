<?php

namespace App\Http\Resources;

use App\Models\StockopnameSubmit;
use Illuminate\Http\Resources\Json\ResourceCollection;

class SubmitCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $submit = StockopnameSubmit::whereNull('stockopname_id')
            ->withCount('detail')
            ->get();

        $sum = array_reduce($submit->toArray(), function ($carry, $item) {
            return $carry + $item['detail_count'];
        });

        return [
            'data' => $this->collection,
            'total_pic' => StockopnameSubmit::whereNull('stockopname_id')->count(),
            'total_detail' => $sum,
        ];
    }
}
