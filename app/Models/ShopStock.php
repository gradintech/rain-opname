<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ShopStock extends Model
{
    use HasFactory;
    public static $COLUMN_STOCK;
    public static $COLUMN_PRODUCT_CODE;
    public static $COLUMN_BRANCH_CODE;
    public static $FILTER_BRANCH;

    // protected $connection = 'mysqlShop';
    /**
     * Specify the connection, since this implements multitenant solution
     * Called via constructor to faciliate testing
     *
     * @param array $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);
        $this->setConnection(config('database.shop_connection'));
        $this->table = env('DB_TABLE_ITEM');
        self::$COLUMN_PRODUCT_CODE = env('DB_TABLE_ITEM_COLUMN_PRODUCT_CODE');
        self::$COLUMN_STOCK = env('DB_TABLE_ITEM_COLUMN_STOCK');
        self::$COLUMN_BRANCH_CODE = env('DB_TABLE_ITEM_COLUMN_BRANCH_CODE');
        self::$FILTER_BRANCH = env('DB_TABLE_ITEM_FILTER_BRANCH');
    }

    /**
      * Scope a query to check if db has branch column or not.
      *
      * @param  \Illuminate\Database\Eloquent\Builder  $query
      * @return \Illuminate\Database\Eloquent\Builder
      */
    public function scopeFilterWarehouseBranch(Builder $query)
    {
        $query->when(self::$COLUMN_BRANCH_CODE, function ($query, $columnName) {
            return $query->where($columnName, self::$FILTER_BRANCH);
        });
    }
}
