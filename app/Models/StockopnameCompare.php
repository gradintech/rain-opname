<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class StockopnameCompare extends Model
{
    use HasFactory;

    public static function getNewComparison($stockopname_id, $stockopname_compare)
    {
        $prev_compare_product_code = count($stockopname_compare) ? $stockopname_compare->pluck('product_code') : collect([]);
        $stockopname = Stockopname::find($stockopname_id);
        $chunk = 10000000;
        $shop_stock = new ShopStock();
        $details = StockopnameSubmit::where('stockopname_id', $stockopname_id)
            ->leftjoin('stockopname_details', 'stockopname_submits.id', '=', 'stockopname_details.stockopname_submit_id')
            ->selectRaw('upper(stockopname_details.product_code) as product_code, COUNT(product_code) as stock')
            ->groupBy('stockopname_details.product_code')
            ->havingRaw('COUNT(stockopname_details.product_code) > ?', [0])
            ->get();

        $detail_chunk = $details->chunk($chunk);
        $compares = collect([]);

        $compares = $compares->merge(self::getExistingProductComparison($detail_chunk, $stockopname_compare, $prev_compare_product_code));
        $compares = $compares->merge(self::getNonExistingProductComparison($detail_chunk, $stockopname_compare, $prev_compare_product_code));

        StockopnameCompare::where('stockopname_id', $stockopname_id)->delete();
        $stockopname->compare()->saveMany($compares);
    }

    public static function getCategorizedStockopname($stockopname_compare, $state = 'web')
    {
        if ($state == 'web') {
            $category = [
                ['name' => 'less', 'title' => 'Kurang', 'color_head' => 'bg-red-500', 'color_row' => 'bg-red-50', 'colspan' => 6, 'sortedResult' => []],
                ['name' => 'more', 'title' => 'Lebih', 'color_head' => 'bg-green-500', 'color_row' => 'bg-green-50', 'colspan' => 6, 'sortedResult' => []],
                // ['name' => 'missed', 'title' => 'Terlewat', 'color_head' => 'bg-yellow-500', 'color_row' => 'bg-yellow-50', 'colspan' => 4, 'sortedResult' => []],
                // ['name' => 'new', 'title' => 'Tidak Terdaftar', 'color_head' => 'bg-blue-500', 'color_row' => 'bg-blue-50', 'colspan' => 4, 'sortedResult' => []],
            ];
        } else {
            $category = [
                ['class' => 'stockopname-less', 'category' => 'less', 'title' => 'Kurang', 'color_head' => '#FF5A5F', 'color_row' => '#ffe6e6', 'sortedResult' => []],
                ['class' => 'stockopname-more', 'category' => 'more', 'title' => 'Lebih', 'color_head' => '#52B788', 'color_row' => '#edf7f3', 'sortedResult' => []],
                // ['class' => 'stockopname-missed', 'category' => 'missed', 'title' => 'Terlewat', 'color_head' => '#FFB30C', 'color_row' => '#fff7e6', 'sortedResult' => []],
                // ['class' => 'stockopname-new', 'category' => 'new', 'title' => 'Tidak Terdaftar', 'color_head' => '#4EA8DE', 'color_row' => '#e9f4fb', 'sortedResult' => []],
            ];
        }

        foreach ($stockopname_compare as $data) {
            if ($data->stockopname > $data->stockdb) {
                // if (!isset($data->stockdb)) {
                //     $category[2]['sortedResult'][] = $data;
                // } else {
                //     $category[1]['sortedResult'][] = $data;
                // }
                $category[1]['sortedResult'][] = $data;
            } elseif ($data->stockopname < $data->stockdb) {
                // if ($data->stockopname == 0) {
                //     $category[2]['sortedResult'][] = $data;
                // } else {
                //     $category[0]['sortedResult'][] = $data;
                // }
                $category[0]['sortedResult'][] = $data;
            }
        }

        return $category;
    }

    public static function getExistingProductComparison($detail_chunk, $stockopname_compare, $prev_compare_product_code)
    {
        $compares = collect([]);
        foreach ($detail_chunk as $detailchunk) {
            $stockdb = ShopStock::select(DB::raw('upper(' . ShopStock::$COLUMN_PRODUCT_CODE . ') as ' . ShopStock::$COLUMN_PRODUCT_CODE), ShopStock::$COLUMN_STOCK)
                ->whereIn(ShopStock::$COLUMN_PRODUCT_CODE, $detailchunk->pluck('product_code'))
                ->filterWarehouseBranch()
                ->get();
            if (isset($stockdb)) {
                $stockdb_product_code = $stockdb->pluck('kode');
                $comparing = $detailchunk->map(function ($detail) use ($stockdb, $stockopname_compare, $stockdb_product_code, $prev_compare_product_code) {
                    $index_product_code = $stockdb_product_code->search($detail->product_code, true);
                    $index_product_code_on_prev_compare = $prev_compare_product_code->search($detail->product_code, true);
                    $compare = new StockopnameCompare();
                    $compare->product_code = $detail->product_code;
                    $compare->stockopname = $detail->stock;
                    $compare->stockdb = is_bool($index_product_code) ? null : $stockdb[$index_product_code][ShopStock::$COLUMN_STOCK];
                    $compare->note = is_bool($index_product_code_on_prev_compare) ? '' : $stockopname_compare[$index_product_code_on_prev_compare]['note'];

                    return $compare;
                });
                $compares = $compares->merge($comparing);
            }
        }

        return $compares;
    }

    public static function getNonExistingProductComparison($detail_chunk, $stockopname_compare, $prev_compare_product_code)
    {
        $stockdb = ShopStock::whereNotIn(ShopStock::$COLUMN_PRODUCT_CODE, $detail_chunk[0]->pluck('product_code'))
            ->where(ShopStock::$COLUMN_STOCK, '>', 0)
            ->filterWarehouseBranch()
            ->get();
        $compares = $stockdb->map(function ($stock) use ($stockopname_compare, $prev_compare_product_code) {
            $index_product_code_on_prev_compare = $prev_compare_product_code->search(strtoupper($stock[ShopStock::$COLUMN_PRODUCT_CODE]), true);
            $compare = new StockopnameCompare();
            $compare->product_code = strtoupper($stock[ShopStock::$COLUMN_PRODUCT_CODE]);
            $compare->stockopname = 0;
            $compare->stockdb = $stock[ShopStock::$COLUMN_STOCK];
            $compare->note = is_bool($index_product_code_on_prev_compare) ? '' : $stockopname_compare[$index_product_code_on_prev_compare]['note'];

            return $compare;
        });

        return $compares;
    }
}
