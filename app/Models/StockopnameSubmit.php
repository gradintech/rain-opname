<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StockopnameSubmit extends Model
{
    use HasFactory;

    public function stockopname()
    {
        return $this->belongsTo(Stockopname::class);
    }

    public function detail()
    {
        return $this->hasMany(StockopnameDetail::class);
    }

    public function detailGrouped()
    {
        return $this->hasMany(StockopnameDetail::class)
            ->selectRaw('stockopname_submit_id, product_code, count(product_code) as count_product')
            ->groupBy('product_code', 'stockopname_submit_id');
    }
}
