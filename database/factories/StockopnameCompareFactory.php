<?php

namespace Database\Factories;

use App\Models\Stockopname;
use App\Models\StockopnameCompare;
use Illuminate\Database\Eloquent\Factories\Factory;

class StockopnameCompareFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = StockopnameCompare::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'stockopname_id' => Stockopname::factory(),
            'product_code' => 'A' . rand(1, 10000),
            'stockopname' => rand(1, 10),
            'stockdb' => rand(1, 10),
            'note' => '',
        ];
    }
}
