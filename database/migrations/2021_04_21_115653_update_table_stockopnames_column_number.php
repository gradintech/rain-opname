<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class UpdateTableStockopnamesColumnNumber extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $results = DB::table('stockopnames')->select('id', 'number', 'created_at')->get();

        foreach ($results as $result) {
            DB::table('stockopnames')
                ->where('id', $result->id)
                ->update(['number' => 'SO/' . date('d-m-Y', strtotime($result->created_at))]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
