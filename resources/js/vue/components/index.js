import Modal from './AppModal'
import FontAwesome from './FontAwesome'

export default {
  install (Vue) {
    Vue.component('Modal', Modal)
    Vue.use(FontAwesome)
  },
}