import axios from 'axios'

axios.defaults.withCredentials = true

const config = {
  baseURL: '/',
  timeout: 3000000, // 30s
  headers: {
    Accept: 'application/json',
    'X-Requested-With': 'XMLHttpRequest',
    'Accept-Language': 'id',
  },
}

const GET = (url, params) => REQUEST({ ...config, method: 'get', url, params })
const POST = (url, data) => REQUEST({ ...config, method: 'post', url, data })
const DELETE = (url, data) => REQUEST({ ...config, method: 'delete', url, data })
const PUT = (url, data) => REQUEST({ ...config, method: 'put', url, data })
const PATCH = (url, data) => REQUEST({ ...config, method: 'patch', url, data })
const REQUEST = conf => {
  return new Promise((resolve, reject) => {
    axios
      .request(conf)
      .then(response => {
        resolve(response.data)
      })
      .catch(error => {
        handlesError(error)
        reject(error)
      })
  })
}

/* https://github.com/axios/axios#handling-errors */
function handlesError (error) {
  if (error.response) {
    /**
     * The request was made and the server responded with a
     * status code that falls out of the range of 2xx
     */
    if (error.response.status === 401) {
      window.location.replace(process.env.VUE_APP_BASE_API + '/login?ref=shop')
    } else if (error.response.status === 503) {
      window.location.replace('/maintenance')
    }
  } else if (error.request) {
    /**
     * The request was made but no response was received.
     * `error.request` is an instance of XMLHttpRequest
     * in the browser and an instance of
     * http.ClientRequest in node.js
     */
  } else {
    /* Something happened in setting up the request that triggered an Error */
  }
}

export default {
  GET,
  POST,
  DELETE,
  PUT,
  PATCH,
  REQUEST,
  config,
}