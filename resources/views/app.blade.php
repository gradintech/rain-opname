<!DOCTYPE html>
<html lang="id">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="theme-color" content="#4EA8DE" />
  <title>{{ env('APP_NAME') }}</title>

  <link rel="stylesheet" href="{{ mix('css/app.css') }}">
  @laravelPWA
</head>
<body>
  <noscript>
    <strong>We're sorry but {{ env('APP_NAME') }} doesn't work properly without JavaScript enabled. Please enable it to continue.</strong>
  </noscript>
  <div id="app"></div>
  <script src="{{ mix('js/app.js') }}"></script>
</body>
</html>