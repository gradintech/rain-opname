<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
<title>Laporan Stok Opname - {{$date}}</title>
  <style type="text/css">
  @page {
      margin: 120px 25px;
  }

  * {
    box-sizing: border-box;
  }

  header {
      position: fixed;
      top: -100px;
      left: 0px;
      right: 0px;
      height: 100px;

      /** Extra personal styles **/
      text-align: center;
      line-height: 35px;
  }

  table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
    padding-left: 5px;
    padding-right: 5px;
  }
  
  /* Create two equal columns that floats next to each other */
  .column {
    float: left;
  }

  /* Clear floats after the columns */
  .row:after {
    content: "";
    display: table;
    clear: both;
  }

  h2, h3 {
    margin: 10px
  }
  </style>
</head>
<body>
  <header style="text-align: center">
    <h2 style="margin: 2px">LAPORAN STOK OPNAME</h2>
    <h3 style="margin: 2px">Cabang {{ env('SHOP_NAME') }}</h3>
    <hr style="border-width:0;height: 3px; background-color: #80bfff">
  </header>

  <main>
    <div class="row">
      <div class="column" style="width: 50%">
        <p style="margin: 0"></p>
      </div>
      <div class="column" style="width: 50%; text-align: right">
        <p style="margin: 0">{{$date}}</p>
      </div>
    </div>
    <div class="row" style="border: 3px solid #80bfff; font-size: 10px">
      <div class="column" style="width: 25%">
        <pre style="padding-left: 20px; margin: 5px">- kurang    = {{count($category[0]['sortedResult'])}}</pre>
        <pre style="padding-left: 20px; margin: 5px">- tidak terdaftar  = {{count($category[2]['sortedResult'])}}</pre>
      </div>
      <div class="column" style="width: 25%">
        <pre style="padding-left: 20px; margin: 5px">- lebih            = {{count($category[1]['sortedResult'])}}</pre>
      </div>
      <div class="column" style="width: 50%; padding-left: 20px">
        <pre style="padding-left: 20px; margin: 5px">* QTY   = Jumlah barang saat di scan</pre>
        <pre style="padding-left: 20px; margin: 5px">* Stock = Jumlah barang di database</pre>
      </div>
    </div>

    @for ($i=0;$i<3;$i++)
      <div class={{$category[$i]['class']}}>
        <h2>{{$category[$i]['title']}}</h2>
        <table style="width:100%">
          <tr style="background: {{$category[$i]['color_head']}}; color: white">
            <th style="width: 5%">No</th>
            @if ($i<2) <th style="width: 25%; text-align: left"> @else <th style="width: 41%; text-align: left"> @endif Kode Barang</th>
            <th style="width: 8%; text-align: right">Qty</th>
            @if ($i<2) <th style="width: 8%; text-align: right">Stock</th> @endif
            @if ($i<2) <th style="width: 8%; text-align: right">Selisih</th> @endif
            <th style="text-align: left">Keterangan</th>
          </tr>
          <?php $j = 1 ?>
          @if (count($category[$i]['sortedResult']))
            @foreach ($category[$i]['sortedResult'] as $data)
              <tr style="background: {{$category[$i]['color_row']}}">
                <td style="text-align: center">{{$j}}</td>
                <td>{{$data->product_code}}</td>
                <td style="text-align: right">{{$data->stockopname}}</td>
                @if ($i<2)<td style="text-align: right">{{$data->stockdb}}</td> @endif
                @if ($i<2) <td style="text-align: right"> <?php echo $i == 0 ? '- ' : '+ ' ?>{{abs($data->stockopname - $data->stockdb)}}</td> @endif
                <td>{{$data->note}}</td>
              </tr>
              <?php $j++ ?>
            @endforeach 
          @else
            @if ($i<2) <td colspan="6"> @else <td colspan="4"> @endif Tidak ada data</td>
          @endif
        </table>
      </div>
    @endfor

    <table style="width:100%; border: hidden">
      <tr>
        <th style="width: 60%; border: hidden"></th>
        <th style="width: 40%; border: hidden">
          <p>Surabaya, {{$today}}</p>
          <p>Penanggung Jawab</p>
          <br>
          <br>
          <br>
          <br>
          <br>
          <hr style="border: 1px dashed black">
        </th>
      </tr>
    </table>
  </main>
</body>
</html>