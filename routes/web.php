<?php

use App\Http\Controllers\FileSystemController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\StockopnameCompareController;
use App\Http\Controllers\StockopnameController;
use App\Http\Controllers\StockopnameDetailController;
use App\Http\Controllers\StockopnameSubmitController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//redirect route bila tidak ditemukan laravel
Route::fallback(function () {
    return view('app');
});

Route::post('stockopname/destroy', [StockopnameController::class, 'destroy'])->name('stockopname.destroy');
Route::get('stockopname/getListDate', [StockopnameController::class, 'getListDate'])->name('stockopname.list-date');
Route::get('stockopname_compare/print/{stockopname_id}/{state?}', [StockopnameCompareController::class, 'print'])->name('stockopname_compare.print');
Route::get('stockopname_compare/report/{stockopname_id}/{state?}', [StockopnameCompareController::class, 'createReport'])->name('stockopname_compare.create-report');
Route::post('stockopname_detail', [StockopnameDetailController::class, 'store'])->name('stockopname_detail.store');
Route::post('stockopname_detail/destroy', [StockopnameDetailController::class, 'destroy'])->name('stockopname_detail.destroy');
Route::get('file_system/download', [FileSystemController::class, 'download'])->name('file_system.download');
Route::post('file_system/update', [FileSystemController::class, 'update'])->name('file_system.update');
Route::get('file_system/getIp', [FileSystemController::class, 'getIp'])->name('file_system.get-ip');

Route::resource('stockopname', StockopnameController::class)->except([
    'destroy',
]);
Route::resource('stockopname_compare', StockopnameCompareController::class);
Route::resource('stockopname_submit', StockopnameSubmitController::class);

Route::resource('home', HomeController::class);
