<?php

namespace Tests\Feature\Controller;

use App\Models\StockopnameDetail;
use App\Models\StockopnameSubmit;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class HomeControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use RefreshDatabase;
    protected $url = '/home';

    public function testHomeControllerIndex()
    {
        //ceate random data using factory
        $submit = StockopnameSubmit::factory()
            ->has(
                StockopnameDetail::factory()
                    ->count(3)
                    ->state(['product_code' => 'A0001*B*XL']),
                'detail'
            )
            ->create();

        // index home
        $this->jsonGet()->assertJson([
            'data' => [
                [
                    'username' => $submit->username,
                    'detail_count' => 3,
                    'detail' => [
                        ['stockopname_submit_id' => $submit->id, 'product_code' => 'A0001*B*XL'],
                        ['stockopname_submit_id' => $submit->id, 'product_code' => 'A0001*B*XL'],
                        ['stockopname_submit_id' => $submit->id, 'product_code' => 'A0001*B*XL'],
                    ],
                ],
            ],
        ]); //result query table stockopname_submits, where stockopname_id null, has many detail, with count detail
    }
}
