<?php

namespace Tests\Feature\Controller;

use App\Models\StockopnameDetail;
use App\Models\StockopnameSubmit;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class StockopnameDetailControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use RefreshDatabase;
    protected $url = '/stockopname_detail';

    public function testStockopnameDetailStore()
    {
        $submit = StockopnameSubmit::factory()
            ->has(
                StockopnameDetail::factory()
                    ->count(3)
                    ->state(['product_code' => 'D100']),
                'detail'
            )
            ->create();

        $form = [
            'stockopname_submit_id' => $submit->id,
            'product_code' => 'D100',
            'new_product_code' => 'D200',
            'qty' => 5,
        ];

        $this->jsonPost($form);

        $this->assertDatabaseHas('stockopname_details', [
            'product_code' => 'D200',
        ]);
    }

    public function testStockopnameDetailDestroy()
    {
        $submit = StockopnameSubmit::factory()
            ->has(
                StockopnameDetail::factory()
                    ->count(3)
                    ->state(['product_code' => 'C100']),
                'detail'
            )
            ->create();

        $form = [
            'stockopname_submit_id' => $submit->id,
            'product_code' => 'C100',
        ];
        $this->url = route('stockopname_detail.destroy', $form);
        $this->jsonPost();

        // check if data on detail with current product code has been completely deleted
        $this->assertDatabaseMissing('stockopname_details', [
            'product_code' => 'C100',
        ]);
    }
}
