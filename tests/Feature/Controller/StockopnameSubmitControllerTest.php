<?php

namespace Tests\Feature\Controller;

use App\Models\StockopnameDetail;
use App\Models\StockopnameSubmit;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class StockopnameSubmitControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use RefreshDatabase;
    protected $url = '/stockopname_submit';

    public function testStockopnameSubmitStore()
    {
        $form = [
            'username' => 'User',
            'items' => ['A100', 'A200', 'A300'],
        ];

        $this->jsonPost($form);

        $expected = [
            'username' => $form['username'],
        ];
        $this->assertDatabaseHas('stockopname_submits', $expected);

        foreach ($form['items'] as $item) {
            $this->assertDatabaseHas('stockopname_details', [
                'product_code' => $item,
            ]);
        }
    }

    public function testStockopnameSubmitShow()
    {
        $detail_count = 3;
        $product_code = 'A0001*B*XL';

        //ceate random data using factory
        $submit = StockopnameSubmit::factory()
            ->has(
                StockopnameDetail::factory()
                    ->count($detail_count)
                    ->state(['product_code' => $product_code]),
                'detail'
            )
            ->create();

        //show saved submit
        $this->url = route('stockopname_submit.show', ['stockopname_submit' => $submit]); //show
        $this->jsonGet()->assertJson([
            'data' => [[
                'id' => $submit->id,
                'username' => $submit->username,
                'detail_grouped' => [
                    ['stockopname_submit_id' => $submit->id, 'product_code' => $product_code, 'count_product' => $detail_count],
                ],
            ]],
        ]); //result query stockopname_submits where id x, has many detail, with count detail
    }

    public function testStockopnameSubmitUpdate()
    {
        $submit = StockopnameSubmit::factory()->create(['username' => 'Old User']);
        $form = [
            'username' => 'New User',
        ];

        $this->jsonPut($form, $submit->id)->assertOk()->assertJson($expected = [
            'id' => $submit->id,
            'username' => $form['username'],
        ]);

        $this->assertDatabaseHas('stockopname_submits', $expected);
    }

    public function testStockopnameSubmitDestroy()
    {
        $submit = StockopnameSubmit::factory()
            ->has(
                StockopnameDetail::factory()
                    ->count(3)
                    ->state(['product_code' => 'L100']),
                'detail'
            )
            ->create();

        $this->url = '/stockopname_submit';
        $this->jsonDelete($submit->id)->assertOk();

        // check if data on submit and detail with current product code has been completely deleted
        $this->assertDatabaseMissing('stockopname_submits', [
            'id' => $submit->id,
        ]);
        $this->assertDatabaseMissing('stockopname_details', [
            'product_code' => 'L100',
        ]);
    }
}
