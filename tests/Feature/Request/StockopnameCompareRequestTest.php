<?php

namespace Tests\Feature\Request;

use App\Models\StockopnameCompare;
use Tests\TestCase;

class StockopnameCompareRequestTest extends TestCase
{
    protected $url = '/stockopname_compare';

    public function setUp(): void
    {
        parent::setUp();

        $this->compare = StockopnameCompare::factory()->create();
    }

    /**
     * Test error message when field is not a string.
     *
     * @return void
     */
    public function testString()
    {
        $form = [
            'oldProductCode' => ['key' => 'value'],
            'newProductCode' => ['key' => 'value'],
            'note' => ['key' => 'value'],
        ];

        $this->jsonPut($form, $this->compare->id)->assertJsonValidationErrors([
            'oldProductCode' => 'The old product code must be a string.',
            'newProductCode' => 'The new product code must be a string.',
            'note' => 'The note must be a string.',
        ]);
    }
}
