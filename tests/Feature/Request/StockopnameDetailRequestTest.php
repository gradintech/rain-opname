<?php

namespace Tests\Feature\Request;

use Tests\TestCase;

class StockopnameDetailRequestTest extends TestCase
{
    protected $url = '/stockopname_detail';

    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     * Test error message when field is not provided or empty.
     *
     * @return void
     */
    public function testRequired()
    {
        $form = [];

        $this->jsonPost($form)->assertJsonValidationErrors([
            'stockopname_submit_id' => 'The stockopname submit id field is required.',
            'product_code' => 'The product code field is required.',
            'new_product_code' => 'The new product code field is required.',
            'qty' => 'The qty field is required.',
        ]);
    }

    /**
     * Test error message when field is not an integer.
     *
     * @return void
     */
    public function testInteger()
    {
        $form = [
            'stockopname_submit_id' => 'some text',
            'qty' => 'some text',
        ];

        $this->jsonPost($form)->assertJsonValidationErrors([
            'stockopname_submit_id' => 'The stockopname submit id must be an integer.',
            'qty' => 'The qty must be an integer.',
        ]);
    }

    /**
     * Test error message when field is not a string.
     *
     * @return void
     */
    public function testString()
    {
        $form = [
            'product_code' => ['key' => 'value'],
            'new_product_code' => ['key' => 'value'],
        ];

        $this->jsonPost($form)->assertJsonValidationErrors([
            'product_code' => 'The product code must be a string.',
            'new_product_code' => 'The new product code must be a string.',
        ]);
    }
}
