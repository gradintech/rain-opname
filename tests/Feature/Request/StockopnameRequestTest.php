<?php

namespace Tests\Feature\Request;

use Tests\TestCase;

class StockopnameRequestTest extends TestCase
{
    protected $url = '/stockopname';

    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     * Test error message when field value invalid selected on rule.
     *
     * @return void
     */
    public function testExist()
    {
        $form = [
            'stockopname_id' => 'some text',
        ];

        $this->jsonPost($form)->assertJsonValidationErrors([
            'stockopname_id' => 'The selected stockopname id is invalid.',
        ]);
    }
}
