<?php

namespace Tests\Feature\Request;

use App\Models\StockopnameSubmit;
use Tests\TestCase;

class StockopnameSubmitRequestTest extends TestCase
{
    protected $url = '/stockopname_submit';

    public function setUp(): void
    {
        parent::setUp();

        $this->submit = StockopnameSubmit::factory()->create();
    }

    /**
     * Test error message when field is not provided or empty.
     *
     * @return void
     */
    public function testRequired()
    {
        $form = [];

        $this->jsonPost($form)->assertJsonValidationErrors([
            'username' => 'The username field is required.',
            'items' => 'The items field is required.',
        ]);
    }

    /**
     * Test error message when field is not an array.
     *
     * @return void
     */
    public function testArray()
    {
        $form = [
            'items' => 'some text',
        ];

        $this->jsonPost($form)->assertJsonValidationErrors([
            'items' => 'The items must be an array.',
        ]);
    }

    /**
     * Test error message when field is not a string.
     *
     * @return void
     */
    public function testString()
    {
        $form = [
            'username' => ['key' => 'value'],
        ];

        $this->jsonPost($form)->assertJsonValidationErrors([
            'username' => 'The username must be a string.',
        ]);
        $this->jsonPut($form, $this->submit->id)->assertJsonValidationErrors([
            'username' => 'The username must be a string.',
        ]);
    }
}
